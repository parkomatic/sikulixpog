package ru.dankovtsev;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.awt.event.KeyEvent;

import static com.sun.javafx.scene.control.skin.Utils.getResource;
import static java.lang.ClassLoader.getSystemResource;

public class MainSikulixStartParkomatic {
    private static final Pattern exit = new Pattern(ClassLoader.getSystemResource("exit.png"));
    private static final Pattern icon = new Pattern(ClassLoader.getSystemResource("icon.png"));
    private static final Pattern menu_mosparking = new Pattern(ClassLoader.getSystemResource("menu_mosparking.png"));
    private static final Pattern menu_mosparking2 = new Pattern(ClassLoader.getSystemResource("menu_mosparking2.png"));
    private static final Pattern vvod_number = new Pattern(ClassLoader.getSystemResource("vvod_number.png"));
    private static final Pattern vvod_pin = new Pattern(ClassLoader.getSystemResource("vvod_pin.png"));
    private static final Pattern vhod = new Pattern(ClassLoader.getSystemResource("vhod.png"));
    private static final Pattern seven = new Pattern(ClassLoader.getSystemResource("seven.png"));
    private static final Pattern MP = new Pattern(ClassLoader.getSystemResource("MP.png"));
    private static final Pattern empty = new Pattern(ClassLoader.getSystemResource("empty.png"));
    private static final Pattern oplatasuccess = new Pattern(ClassLoader.getSystemResource("oplatasucces.png"));
    private static final Pattern cars = new Pattern(ClassLoader.getSystemResource("cars.png"));
    private static final Pattern ready = new Pattern(ClassLoader.getSystemResource("ready.png"));
    private static final Pattern delete = new Pattern(ClassLoader.getSystemResource("delete.png"));
    private static final Pattern cars2 = new Pattern(ClassLoader.getSystemResource("cars2.png"));
    private static final Pattern blackcar = new Pattern(ClassLoader.getSystemResource("car_in_cars.png"));
    private static final Pattern plus = new Pattern(ClassLoader.getSystemResource("plus.png"));
    private static final Pattern A = new Pattern(ClassLoader.getSystemResource("A.png"));
    private static final Pattern oplata = new Pattern(ClassLoader.getSystemResource("oplata.png"));
    private static final Pattern np = new Pattern(ClassLoader.getSystemResource("number_parking.png"));
    private static final Pattern but_oplata = new Pattern(ClassLoader.getSystemResource("but_oplata.png"));

    public static void main(String[] args) {
        String number=args[0];
        String pin =args[1];
        String numberCAR= args[2];
        String numberPARK= args[3];
        String time= args[4];
       /* String number="9853145466";
        String pin ="3101";
        String numberCAR="H000AA77";
        String numberPARK= "4021";
        String time= "1";*/
        Screen screen =new Screen();
        boolean checkLK=false;
        boolean checkOPLATA=false;
        checkLK=checkLOgInLK();

        if (!checkLK)
        {
            checkScreen(screen,empty, 10);
            checkLK=enterInApplication(number,pin);
        }
        if (checkLK)
        {

            System.out.println("You are in LK");
            checkOPLATA=oplataDo(numberCAR,numberPARK);
        }
        if (checkOPLATA)
        {
            System.out.println("parking was successful");
        }

    }
    //проверка на нахожденнии в личном кабинете
    public static boolean checkLOgInLK()
    {
        Screen screen = new Screen();
        boolean check;
        checkScreen(screen,menu_mosparking2, 30);
        try {
            screen.wait(icon,10);
            screen.wait(exit,10);
            checkScreen(screen,empty, 10);
            check=true;

        } catch (FindFailed findFailed) {
            //findFailed.printStackTrace();
            check=false;
        }
        return check;
    }


    //поиск изображение на экране
    public static void checkScreen(Screen screen,Pattern img, int sec) {
        //Screen screen = new Screen();
        try {
            screen.wait(img.similar((float) 0.90), sec).click();


        } catch (FindFailed exeption) {

        }
    }

    //авторизация в личном кабинете
    public static boolean enterInApplication(String number,String pin){
        Screen screen = new Screen();
        boolean checkEnter =false;
        checkScreen(screen,MP , 5);
        checkScreen(screen,seven , 5);
        enterButton(screen, KeyEvent.VK_RIGHT);
        for(int i=0; i<10;i++)
        {
            enterButton(screen,KeyEvent.VK_BACK_SPACE);
        }
        // screen.type(args[0]);
        screen.type(number);
        enterButton(screen,KeyEvent.VK_ENTER);
        checkScreen(screen,vvod_pin , 5);
        for(int i=0; i<4;i++)
        {
            enterButton(screen,KeyEvent.VK_DELETE);
            //screen.keyDown(KeyEvent.VK_DELETE);
            // screen.keyUp(KeyEvent.VK_DELETE);


        }
        //screen.type(args[1]);
        screen.type(pin);
        enterButton(screen,KeyEvent.VK_ENTER);
        checkScreen(screen,vhod , 5);
        stopprog(1500);
        checkEnter=checkLOgInLK();
        return checkEnter;
    }

    //происходит оплата парковки
    public static boolean oplataDo(String numberCAR,String numberPARK)
    {
        Screen screen = new Screen();
        try {
            screen.wait(menu_mosparking2,5);
            while (!flagLog(screen,icon,5))
            {

                checkScreen(screen,menu_mosparking2, 5);

            }
            //checkScreen(screen, menu_mosparking , 5);

        } catch (FindFailed findFailed) {

        }
        finally
        {
            try {
                screen.wait(icon,5);
            } catch (FindFailed findFailed) {
                findFailed.printStackTrace();
            }
            checkScreen(screen,cars , 5);
            checkScreen(screen,cars2 , 5);
        }
        while (flagLog(screen,blackcar,5))
        {
            checkScreen( screen,blackcar, 5);
            screen.rightClick();
            checkScreen(screen,delete , 5);
            checkScreen(screen,ready , 5);
            stopprog(2000);
        }
        checkScreen(screen,plus , 5);
        checkScreen(screen,A , 5);
        screen.type(numberCAR);
        checkScreen(screen,ready , 5);
        stopprog(1500);
        checkScreen(screen,menu_mosparking2 , 5);
        checkScreen(screen,oplata , 5);
        checkScreen(screen,np , 5);
        stopprog(1000);
        screen.type(numberPARK);
        checkScreen(screen,but_oplata , 5);
        checkScreen(screen,ready , 5);
        try {
            screen.wait(oplatasuccess,10);
            return true;
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            return false;
        }

    }



    //имитация нажатия на кнопку
    public static void enterButton(Screen screen,int k) {
        // Screen screen = new Screen();
        screen.keyDown(k);
        screen.keyUp(k);

    }
    //остановка на определеное время
    public static void stopprog(int sec) {
        try {
            Thread.sleep(sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //проверка машин в истории
    public static boolean flagLog(Screen screen,Pattern img, int sec) {
        // Screen screen = new Screen();
        try {
            screen.wait(img, sec);
            return true;
        } catch (FindFailed exeption) {
            return false;
        }
    }

}